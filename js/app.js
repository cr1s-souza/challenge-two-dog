﻿
var vmd;
function DogViewModel() {
    var self = this;
    self.allBreeds = ko.observableArray([]);
    self.detalhesDogSalvo = ko.observableArray([]);
    self.msgSucesso = ko.observable(false);
    self.msgAlerta = ko.observable(false);

    self.getDogImage = function(selectedBreed){
        if(selectedBreed){
            dogParams = selectedBreed.valor.replace(/-/g,'/');
            $.getJSON("https://dog.ceo/api/breed/"+ dogParams +"/images/random", function(response){
                $("#url-dog-image").val(response.message);
            });
        }
    }
    
    self.loadBreeds = function(){
        $.getJSON("https://dog.ceo/api/breeds/list/all", function(response){
            var breeds = response.message;

            $.each(breeds, function(dog, breed){
                if(breeds[dog].length >= 1){
                    for(i = 0; i < breeds[dog].length; i++){
                        self.allBreeds.push({valor: dog +'-'+ breeds[dog][i], nome: breeds[dog][i] +' '+ dog});
                    }
                }
                else if(breeds[dog].length < 1){
                    self.allBreeds.push({valor: dog, nome: dog});
                }
            });
        });
        self.detalhesDogSalvo(localStorage.getItem('detalhesDog') ? JSON.parse(localStorage.getItem('detalhesDog')) : []);
    }


    self.salvarDogCustom = function(){
        var dogImageUrl = $("#url-dog-image"            ).val();
        var breedValue  = $("#raca-dog option:selected" ).val();
        var breedName   = $("#raca-dog option:selected" ).text();
        var nomeDog     = $("#nome-dog"                 ).val();
        var fontColor   = $("#cor-fonte"                ).val();
        var fontFamily  = $("#familia-fonte"            ).val();

        if(fontFamily){
            switch(fontFamily){
                case 'roboto': fontFamily = 'roboto-font'; break
                case 'lato': fontFamily = 'lato-font'; break
                case 'bebas-neue': fontFamily = 'bebas-neue-font'; break
                case 'indie-flower': fontFamily = 'indie-flower-font'; break
                case 'lobster': fontFamily = 'lobster-font'; break
            }
        }

        if(breedName.indexOf('Selecione') == -1){
            self.detalhesDogSalvo.push({ 
                dogUrl: dogImageUrl, 
                racaValor: breedValue, 
                racaNome: breedName, 
                nomeDog: nomeDog, 
                corFonte: fontColor, 
                familiaFonte: fontFamily ,
                datetime: new Date().toString().split('GMT')[0]
            })
            
            self.updateStorage();
        }else{
            self.msgAlerta(true);
            setTimeout(function(){
                self.msgAlerta(false);
            },2000)
        }
    }

    self.updateStorage = function(){
        localStorage.removeItem('detalhesDog');
        localStorage.setItem('detalhesDog', JSON.stringify(self.detalhesDogSalvo()))
        
        self.msgSucesso(true);
        setTimeout(function(){
            self.msgSucesso(false);
        },2000)
    }

    self.limparDogsSalvos = function(){
        localStorage.removeItem('detalhesDog');
        self.detalhesDogSalvo([]);
    }


    self.loadBreeds();
    ko.applyBindings(self, $("#list-dog-custom")[0]);

};

$(document).ready(function () {
    vmd = new DogViewModel();
})